# Anagolay Docker images for rust and substrate

To save time on CI we have this box that is firstly intended to be used as a buildbox for the Anagolay network. Although it can be used for any Substrate based chain.

The box does not contain any Anagolay specific code, it contains the sane environment from the rust version, toolchain and cargo packages.

https://rust-lang.github.io/rustup-components-history/

## Images:

-   Rust base image
    -   [1-bullseye](https://hub.docker.com/layers/191319352/anagolay/rust/1-bullseye/images/sha256-1189b4b0a1be660e1778f166060a3d39c898758519c9015a8722d645164c2fdd?context=repo)
-   Base image for Anagolay Operation
    -   [rust_stable-2022-01-20_bullseye](https://hub.docker.com/layers/191317642/anagolay/operation/rust_stable-2022-01-20_bullseye/images/sha256-4fcfcf88b3efef4b50fd193a087fd11cbcc9ad85ec62a7bf28fa4b5c5db81b37?context=repo)

## Packages to consider installing for `full-workspace

-   https://brew.sh/
-   https://github.com/xxxserxxx/gotop
-   htop
-   xclip
-   https://github.com/sharkdp/bat

## Debug

-   `export COMPOSE_DOCKER_CLI_BUILD=0 && export DOCKER_BUILD=0 && export DOCKER_BUILDKIT=0`
-   `export COMPOSE_DOCKER_CLI_BUILD=1 && export DOCKER_BUILD=1 && export DOCKER_BUILDKIT=1`
-

## Notes and pacakges

### Cargo make (makers)

Makers 0.35.9 - https://github.com/sagiegurari/cargo-make/releases/download/0.35.9/cargo-make-v0.35.9-x86_64-unknown-linux-musl.zip

Binaries in the zip file:

-   `makers` CID is `bafybeigocpf3bgi24ytoeosa4muxltoxsw3ftbzq2t7zvpwrw2k6r6bqt4`
-   `cargo-make` CID is `bafybeif5f2lrytncjy6zye7z2fe5ca4ani5wjy535cnccrdxmmavme5h5i`

### Sccache

```dockerfile
# Rehosted binary for this https://github.com/mozilla/sccache/releases/download/v0.2.15/sccache-v0.2.15-x86_64-unknown-linux-musl.tar.gz
ADD https://ipfs.anagolay.network/ipfs/bafybeibsxtln53slskrmv6avwnjofxu5qfpmawezh7xzxuxhrhpe23nkym /usr/local/bin/sccache

RUN chmod +x /usr/local/bin/sccache \
 && sccache --version
```

### Wasm-pack

Version https://github.com/rustwasm/wasm-pack/releases/download/v0.10.2/wasm-pack-v0.10.2-x86_64-unknown-linux-musl.tar.gz
CID: `bafybeihxmsqdck7os7pplccqmqe2mrilp5ftsb6s3jxa2qyoih3p5akboa`

### WASM bindgen

Version: https://github.com/rustwasm/wasm-bindgen/releases/download/0.2.79/wasm-bindgen-0.2.79-x86_64-unknown-linux-musl.tar.gz

```sh
» ipfs add --cid-version=1 --pin=true --quieter wasm-bindgen
bafybeidmziznt3q7v2dawkztbyh26zj4mic5j73uyoxqv4j6skulipmtfi

» ipfs add --cid-version=1 --pin=true --quieter wasm2es6js
bafybeihdozvvef5dgd2he2gfpqywpekp6mykohjo3q7e76deuy364rgcyu

» ipfs add --cid-version=1 --pin=true --quieter wasm-bindgen-test-runner
bafybeifyjsryf4kddtpnr6iz6cdaknyp5mhqvke5wthc2c5nishmtrxutm
```

### Binaryen

Version https://github.com/WebAssembly/binaryen/releases/download/version_105/binaryen-version_105-x86_64-linux.tar.gz

```
> ipfs add --cid-version=1 --pin=true --recursive -w .
added bafybeibjqr6q4nrlnvyozltdf25mhgsrxbnw4cuh37d7s6f74vqnfmi54a binaryen-version_105/bin/wasm-as
added bafybeiglueeruhzhdei6n2tq7vpftp6eyrsqgjrxyxshczmb6g6fnsqv74 binaryen-version_105/bin/wasm-ctor-eval
added bafybeibio46yei52qpbl52fafdachwpexutd6qety54oylosbjzbiibb54 binaryen-version_105/bin/wasm-dis
added bafybeiblqqxj5wf27bgwedr33klcdyxh23tbfmntioeo7jvlvtg4nxlnyy binaryen-version_105/bin/wasm-emscripten-finalize
added bafybeigemaqmffxqzqsxxpkbdpra76dk4hat4vqn3plvggyigzek5wsewa binaryen-version_105/bin/wasm-fuzz-types
added bafybeifcmaouwh5spaeqw3sjjbrmxtyaofttmrp4nxarjlhvuxs77vssqe binaryen-version_105/bin/wasm-metadce
added bafybeidrnyhfycqhc2ckqyzef7zt6r5svkjbimzxfkbqkps7bou5oeff6m binaryen-version_105/bin/wasm-opt
added bafybeibusz6utun3ho6qyb6yigcka4zeol6zunjfzzxvdukturzp3aa5em binaryen-version_105/bin/wasm-reduce
added bafybeiamuvfml2t3bsnz722j6aqz2lfimrvzufwxaj65h7ilonbjiwxrye binaryen-version_105/bin/wasm-shell
added bafybeihsryqobhnk3dtlg44ys2iokoexgrlmko4l3v2wikqkoee3xxlw3u binaryen-version_105/bin/wasm-split
added bafybeigctt2i4drsru2kdbbkogz66tthlvr64gejy56cxbg6jdylwtsnru binaryen-version_105/bin/wasm2js
added bafkreifmahoz42qwgkpzajlnmueikaerejjn2faoumyub4mi2wltiqhitu binaryen-version_105/include/binaryen-c.h
added bafkreiebalflzc6cjllggf5egzjbuvflj27nqgmvgeadzolvivpolkpvn4 binaryen-version_105/include/wasm-delegations.def
added bafybeidkjm3o4tw53qogjckhipbmzwwvm7pzaithfkcmoqgmvcql24537y binaryen-version_105/lib/libbinaryen.a
added bafybeicwx2ckkaz4zxe33tlo4nzerpuscsh3vttgr4nvp5deu6nc2ga7f4 binaryen-version_105/bin
added bafybeid7n5ruz4awyhtu4k4oc5tu3smxdyxrfmwuwcqwjy4pt4e63j7lqm binaryen-version_105/include
added bafybeibj4uibnlkixg6k26rmngg3yazfzek2jj5nipzvixaazobvdc74nm binaryen-version_105/lib
added bafybeifmzfugvyz6dtfpt3yf6ark5eoe6ffs3h3jal3mhidcmbhqbfyiwe binaryen-version_105
added bafybeieodl656kgt5tmto5zwdalwesnwl5cruvouryv43wcewaxteviuhu
```
