#!/usr/bin/env bash
set -x
# -a part will allow the sourcing of the env
set -a
set -o errexit

echo "THIS SHOULD NOT BE USED IN PRODUCTION!!!"
echo "IT IS ONLY FOR LOCAL BUILDING OF THE PRODUCTION IMAGE"
echo "IT WILL BE REMOVED AT SOME POINT"

RUST_TOOLCHAIN=stable-x86_64-unknown-linux-gnu

# we might have idiyanale too
PROJECT_ROOT=$(git rev-parse --show-toplevel)
cd $PROJECT_ROOT

PROJECT=${1:-"workspaces-operation"}
GIT_LATEST_REVISION=${2:-$(git rev-parse --short HEAD)}
FULL_IMAGE_NAME_WITH_SHORT_HASH="anagolay/$PROJECT:$GIT_LATEST_REVISION"
FULL_IMAGE_NAME_LATEST="anagolay/$PROJECT:latest"

OCI_REGISTRY="docker.io"

# source $PROJECT_ROOT/.env

echo "Building the base image ..."
docker build \
	--build-arg GIT_LATEST_REVISION=$GIT_LATEST_REVISION \
	--build-arg RUST_TOOLCHAIN=$RUST_TOOLCHAIN \
	--tag $OCI_REGISTRY/$FULL_IMAGE_NAME_WITH_SHORT_HASH \
	--file $PROJECT_ROOT/workspaces/operation/Dockerfile .

echo "Tag latest ..."
docker tag FULL_IMAGE_NAME_WITH_SHORT_HASH $FULL_IMAGE_NAME_LATEST
