ARG VARIANT=bullseye
FROM mcr.microsoft.com/vscode/devcontainers/rust:1-${VARIANT} as base

ARG USER_UID=${UID:-1000}
ARG SETUP_MODE=normal
ARG RUST_TOOLCHAIN="stable-2022-01-20"

ARG TMP_DIR=/tmp/anagolay

ENV USERNAME=vscode \
	RUST_BACKTRACE=1 \
	CARGO_HOME=/usr/local/cargo \
	RUSTUP_HOME=/usr/local/rustup \
	RUSTUP_PROFILE=default \
	PATH=${CARGO_HOME}/bin:${PATH}

ARG NODE_VERSION="16"

ADD https://deb.nodesource.com/setup_$NODE_VERSION.x /tmp/nodesource_setup.sh 

RUN bash /tmp/nodesource_setup.sh \
	&& apt-get update && export DEBIAN_FRONTEND=noninteractive \
	&& apt-get install -y --no-install-recommends \
	git \
	nodejs \
	tree \
	neovim \
	tmux \
	silversearcher-ag \
	libssl-dev \
	clang \
	cmake \
	libclang-dev \
	libffi-dev \
	cmake \
	musl-tools \
	&& apt autoremove -y \
	&& apt-get clean -y && rm -rf /var/lib/apt/lists/*

# this is the v6.30.0
ADD https://ipfs.anagolay.network/ipfs/QmVwR17T5oT4SsH1gb8T9L9gHe5CsJz2iwhbhWCwPPHgHR /usr/local/bin/pnpm
# add the gpg remote signer CLI
ADD https://ipfs.anagolay.network/ipfs/QmVwR17T5oT4SsH1gb8T9L9gHe5CsJz2iwhbhWCwPPHgHR /usr/local/bin/remote-signer 
ADD https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh /tmp/brew-install.sh

RUN chown :${USER_UID} /usr/local/bin/pnpm \
	&& chmod 755 /usr/local/bin/pnpm \ 
	&& chown :${USER_UID} /usr/local/bin/remote-signer\
	&& chmod 755 /usr/local/bin/remote-signer\ 
	&& chmod 755 /tmp/brew-install.sh 

RUN mkdir -p $TMP_DIR

ADD https://github.com/mozilla/sccache/releases/download/v0.2.15/sccache-v0.2.15-x86_64-unknown-linux-musl.tar.gz $TMP_DIR/
# ADD https://github.com/rustwasm/wasm-bindgen/releases/download/0.2.79/wasm-bindgen-0.2.79-x86_64-unknown-linux-musl.tar.gz $TMP_DIR/
# ADD https://github.com/rustwasm/wasm-pack/releases/download/v0.10.2/wasm-pack-v0.10.2-x86_64-unknown-linux-musl.tar.gz $TMP_DIR/
# ADD https://github.com/sagiegurari/cargo-make/releases/download/0.35.8/cargo-make-v0.35.8-x86_64-unknown-linux-musl.zip $TMP_DIR/


RUN tar xvf $TMP_DIR/sccache-v0.2.15-x86_64-unknown-linux-musl.tar.gz -C $TMP_DIR
RUN mv $TMP_DIR/sccache-v0.2.15-x86_64-unknown-linux-musl/sccache $CARGO_HOME/bin/ \
	&& chmod +x $CARGO_HOME/bin/sccache \
	&& chown :${USER_UID} $CARGO_HOME/bin/sccache

## install the stuff fof the user
FROM base




# RUN tar xvf $TMP_DIR/wasm-pack-v0.10.2-x86_64-unknown-linux-musl.tar.gz -C $TMP_DIR
# RUN mv $TMP_DIR/wasm-pack-v0.10.2-x86_64-unknown-linux-musl/wasm-pack $CARGO_HOME/bin/
# RUN unzip $TMP_DIR/cargo-make-v0.35.8-x86_64-unknown-linux-musl.zip -d $TMP_DIR
# RUN mv $TMP_DIR/cargo-make-v0.35.8-x86_64-unknown-linux-musl/cargo-make $CARGO_HOME/bin/
# RUN mv $TMP_DIR/cargo-make-v0.35.8-x86_64-unknown-linux-musl/makers $CARGO_HOME/bin/
# RUN tar xvf $TMP_DIR/wasm-bindgen-0.2.79-x86_64-unknown-linux-musl.tar.gz -C $TMP_DIR
# RUN mv $TMP_DIR/wasm-bindgen-0.2.79-x86_64-unknown-linux-musl/wasm* $CARGO_HOME/bin/ 


# && rm -rf $TMP_DIR \




# switch to the user
USER $USERNAME

ENV SCCACHE_LOG=debug
ENV RUSTC_WRAPPER=$CARGO_HOME/bin/sccache
ENV SCCACHE_DIR=$HOME/.cache/sccache
RUN echo $HOME \
	&& whoami \ 
	&& groups \
	&& ls -lah /home/$USERNAME \
	&& ls -lah /home/


# RUN pnpm env use --global $NODE_VERSION  \
#   && pnpm add -g @microsoft/rush 

# RUN NONINTERACTIVE=1 HOMEBREW_PREFIX_DEFAULT=$HOME bash /tmp/brew-install.sh
# ENV PATH=/home/linuxbrew/.linuxbrew/bin:$PATH
# RUN   eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)" \
# && brew install sccache

RUN sccache -h
# rust part 


# RUN rustup toolchain install $RUST_TOOLCHAIN \
#   && rustup default $RUST_TOOLCHAIN \
#   && rustup target add wasm32-unknown-unknown --toolchain $RUST_TOOLCHAIN \
#   && rustup component add rustfmt --toolchain $RUST_TOOLCHAIN  \
#   && rustup component add rust-std --target wasm32-unknown-unknown --toolchain $RUST_TOOLCHAIN 

# # RUN cargo install sccache



# # RUN cargo install http-server
RUN sccache --show-stats
# # RUN cargo install --force taplo-cli rusty-hook wasm-bindgen-cli wasm-pack cargo-make


# # smoke test
# RUN rustup --version \
#   && rustc --version \
#   && cargo --version \ 
#   && git --version \
#   && tar --version \
#   && node --version \
#   && pnpm --version \
#   && wasm-bindgen --version \
#   && makers --version

