#!/usr/bin/env bash
set -x

root="$(git rev-parse --show-toplevel)"

full_path=$(realpath $0)
dir_path=$(dirname $full_path)

VARIANT=${1:-"bullseye"}
RUST_TOOLCHAIN=${2:-"stable-2022-02-24"}

# build the dependency first
bash $root/base/rust/build.sh

docker build \
	--build-arg VARIANT=${VARIANT} \
	--build-arg RUST_TOOLCHAIN=${RUST_TOOLCHAIN} \
	--tag anagolay/operation:rust_${RUST_TOOLCHAIN}_${VARIANT} \
	--file $dir_path/Dockerfile \
	.

docker push docker.io/anagolay/operation:rust_${RUST_TOOLCHAIN}_${VARIANT}
